#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: clickhouse_table

short_description: Create or alter ClickHouse tables on a remote host

version_added: "2.4"

description:
    - "If the table does not exist on the remote host, it will be created according to columns and engine definitions."
    - "If the table exists on the remote host but differs from provided columns definition,
      it will be altered accordingly."
    - "Note that ClickHouse engine must support schema changes. For example, you cannot alter columns
      included into primary key of MergeTree engine."

options:
    name:
        description:
            - Table name
        required: true
        aliases:
            - table
    database:
        description:
            - Database name
        required: false
        default: default
        aliases:
            - db
    state:
        description:
            - Indicates the desired table state.
        choices: [ "absent", "present" ]
    columns:
        description:
            - "List of dictionaries, each one describes a single column. Keys 'name', 'type',
              'default_expression', 'default_kind' are supported. See EXAMPLES section below."
        required: false
    engine:
        description:
            - "Engine definition in form of 'EngineName(engine_param1, engine_param2, ...)'."
            - "See U(https://clickhouse.yandex/docs/en/table_engines/) for details."
        required: false
    url:
        description:
            - "Url of ClickHouse server, with protocol (http or https)."
        required: true
    create_db:
        description:
            - "Create database if it does not exist."
        default: true
        required: false

author:
    - Anton Alekseyev (@Akint)
'''

EXAMPLES = '''
# Create simple MergeTree table
- set_fact:
    database: default
    table: requests
    columns:
      - name: date
        type: Date
        default_kind: DEFAULT
        default_expression: today()
      - name: datetime
        type: DateTime
        default_kind: DEFAULT
        default_expression: now()
      - name: request_uri
        type: String
      - name: request_time_ms
        type: UInt32
      - name: http_status
        type: UInt16

- name: Create table
  clickhouse_table:
    name: "_{{ table }}"
    database: "{{ database }}"
    columns: "{{ columns }}"
    engine: MergeTree(date, (request_uri), 8192)
    url: http://localhost:8123

- name: Create distributed table looking at our table
  clickhouse_table:
    name: "{{ table }}"
    database: "{{ database }}"
    columns: "{{ columns }}"
    engine: "Distributed(your_cluster, {{ database }}, _{{ table }})"
    url: http://localhost:8192
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url
import json
import re


class CHClient(object):
    def __init__(self, module):
        self.url = module.params['url']
        self._module = module
        self._identifier_name_regexp = '[a-zA-Z_][0-9a-zA-Z_]*'
        self.identifier_name_regexp = '^' + self._identifier_name_regexp + '$'
        # There could be nested data structures, their names look like `field_name.subfield_name`
        self.column_name_regexp = "^{0}(?:\.{0})?$".format(self._identifier_name_regexp)

    def _validate_regular_identifier_name(self, identifier_name):
        if not re.match(self.identifier_name_regexp, identifier_name):
            self._module.fail_json(msg="Invalid identifier name: %s" % identifier_name)

    def _validate_columns_names(self, columns):
        for column in columns:
            column_name = column.name
            if not re.match(self.column_name_regexp, column_name):
                self._module.fail_json(msg="Invalid column name: %s" % column_name)

    def execute_query(self, query):
        response, info = fetch_url(self._module, self.url, method='POST', data=query)
        if info['status'] < 0:
            self._module.fail_json(msg=info['msg'])
        if info['status'] >= 400:
            self._module.fail_json(msg=info['body'])
        return response.read()

    def _parse_query_result_as_json(self, query_result):
        try:
            return json.loads(query_result)
        except Exception as e:
            self._module.fail_json(msg="Failed to parse query result as JSON: %s" % e)

    def get_existing_columns(self, db_name, table_name):
        """Retrieves information about existing columns and returns it as a list of CHColumn instances"""
        self._validate_regular_identifier_name(db_name)
        self._validate_regular_identifier_name(table_name)
        query = "SELECT database, table, name, type, default_expression, default_kind " \
                "FROM system.columns " \
                "WHERE database = '%s' AND table = '%s' " \
                "FORMAT JSON" % (db_name, table_name)
        query_result = self.execute_query(query)
        columns = self._parse_query_result_as_json(query_result)['data']
        return [CHColumn(column) for column in columns]

    def db_exists(self, db_name):
        """Checks whether database exists or not"""
        self._validate_regular_identifier_name(db_name)
        query = "SELECT count() as c FROM system.databases WHERE name = '%s' FORMAT JSON" % db_name
        query_result = self.execute_query(query)
        db_count = int(self._parse_query_result_as_json(query_result)['data'][0]['c'])
        return bool(db_count > 0)

    @staticmethod
    def _generate_column_declaration_string(column):
        res = "`%s` %s" % (column.name, column.type)
        if column.default_kind and column.default_expression:
            res += " %s %s" % (column.default_kind, column.default_expression)
        return res

    def generate_create_db_query(self, table):
        self._validate_regular_identifier_name(table.db_name)
        query = "CREATE DATABASE `%s`;" % table.db_name
        return query

    def generate_drop_table_query(self, table):
        self._validate_regular_identifier_name(table.db_name)
        self._validate_regular_identifier_name(table.name)
        query = "DROP TABLE `%s`.`%s`;" % (table.db_name, table.name)
        return query

    def generate_create_table_query(self, table):
        self._validate_regular_identifier_name(table.db_name)
        self._validate_regular_identifier_name(table.name)
        self._validate_columns_names(table.columns)
        # TODO: Validate engine
        query = "CREATE TABLE `%s`.`%s` (\n" % (table.db_name, table.name)
        query += ",\n".join("    " + self._generate_column_declaration_string(x) for x in table.columns)
        query += "\n) ENGINE = %s;" % table.engine
        return query

    @staticmethod
    def _generate_drop_column_action(column):
        return "DROP COLUMN `%s`" % column.name

    def _generate_modify_column_action(self, column):
        return "MODIFY COLUMN " + self._generate_column_declaration_string(column)

    def _generate_add_column_action(self, column):
        alter_cmd = "ADD COLUMN " + self._generate_column_declaration_string(column)
        if column.prev_column_name:
            alter_cmd += " AFTER `%s`" % column.prev_column_name
        return alter_cmd

    def _generate_invalid_column_action(self, column):
        self._module.fail_json(msg="Cannot generate alter column action for %s (%s)"
                                   % (column.name, column.alter_type))

    def generate_alter_query(self, table, altered_columns):
        self._validate_regular_identifier_name(table.db_name)
        self._validate_regular_identifier_name(table.name)
        self._validate_columns_names(altered_columns)
        actions = []
        for column in altered_columns:
            alter_method_name = "_generate_%s_column_action" % column.alter_type
            generate_alter_column_action = getattr(self, alter_method_name, self._generate_invalid_column_action)
            action = generate_alter_column_action(column)
            actions.append(action)
        query = None
        if actions:
            query = "ALTER TABLE `%s`.`%s`\n" % (table.db_name, table.name)
            query += ",\n".join("    " + x for x in actions)
            query += ";"
        return query


class CHTableDiff(object):
    """Difference between two ClickHouse tables.
    Attributes:
        queries: Queries needed to transform the first table into the second one.
        before: Existing table creation query, empty if the table doesn't exist yet.
                This is not exactly the query existing table was created with.
                We are using engine provided by user instead of the real one and there are two main reasons for that:
                    1. there is no convenient way to retrieve existing table's engine;
                    2. we cannot change engine of existing table anyway.
                So we are interested in columns' change only and engine stands here as a placeholder.
        after: Resulting table creation query, empty if the table is to be deleted.
        prepared: 'queries' merged to a single string ready to be printed.
    Attributes 'before', 'after' and 'prepared' are needed by ansible module to print diff in --diff mode."""

    def __init__(self, ch_client, existing_table, desired_table):
        self.before = ""
        self.after = ""
        self.queries = []
        self.prepared = ""
        self.existing_table = existing_table
        self.desired_table = desired_table
        self._ch_client = ch_client
        self._attrs_to_return = ['before', 'after', 'prepared']

    def __bool__(self):
        return bool(self.queries)

    __nonzero__ = __bool__

    def __iter__(self):
        """Returns generator suitable for Ansible module result's diff section"""
        for key in self._attrs_to_return:
            yield (key, getattr(self, key, ""))

    def set_type(self, type):
        if type == "create" or type == "alter":
            self.after = self._ch_client.generate_create_table_query(self.desired_table) + "\n"
        if type == "drop" or type == "alter":
            self.before = self._ch_client.generate_create_table_query(self.existing_table) + "\n"

    def set_queries(self, queries):
        self.queries = queries
        self.prepared = "\n".join(queries)


class CHColumn(object):
    def __init__(self, column_dict):
        self.name = column_dict['name']
        self.type = column_dict['type']
        self.default_kind = column_dict.get('default_kind', "")
        self.default_expression = column_dict.get('default_expression', "")
        self.prev_column_name = ""
        self.alter_type = ""
        self._attrs_to_compare = ['name', 'type', 'default_expression', 'default_kind']

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return all(str(self.__dict__.get(key, '')) == str(other.__dict__.get(key, ''))
                       for key in self._attrs_to_compare)
        return False

    def __ne__(self, other):
        return not self.__eq__(other)


class CHTable(object):
    def __init__(self, module, ch_client, existing=False):
        self._module = module
        self._ch_client = ch_client
        self.db_name = module.params['database']
        self.name = module.params['table']
        self.engine = module.params['engine']
        if existing:
            self.columns = []
            self.exists = False
            self.db_exists = self._ch_client.db_exists(self.db_name)
            if self.db_exists:
                self.columns = self._ch_client.get_existing_columns(self.db_name, self.name)
                self.exists = bool(self.columns)
        else:
            self.columns = [CHColumn(column) for column in module.params['columns']]
            self.exists = True if module.params['state'] == "present" else False


class CHTableDiffCalculator(object):
    def __init__(self, module, ch_client):
        self._module = module
        self._ch_client = ch_client
        self.create_db = module.params['create_db']

    def calculate(self, existing_table, desired_table):
        diff = CHTableDiff(self._ch_client, existing_table, desired_table)
        if desired_table.exists:
            if existing_table.exists:
                diff_type = 'alter'
            else:
                diff_type = 'create'
        else:
            if existing_table.exists:
                diff_type = 'drop'
            else:
                diff_type = None
        if diff_type:
            build_queries_method_name = "_build_%s_table_queries" % diff_type
            build_queries = getattr(self, build_queries_method_name)
            queries = build_queries(existing_table, desired_table)
            diff.set_type(diff_type)
            diff.set_queries(queries)
        return diff

    @staticmethod
    def _columns_to_dict(columns):
        return {column.name: column for column in columns}

    def _build_create_table_queries(self, existing_table, desired_table):
        queries = []
        if not existing_table.db_exists:
            if not self.create_db:
                self._module.fail_json(
                    msg="Database '%s' does not exist and 'create_db' is set to False" % existing_table.db_name)
            queries.append(self._ch_client.generate_create_db_query(desired_table))
        queries.append(self._ch_client.generate_create_table_query(desired_table))
        return queries

    def _find_missing_or_modified_columns(self, existing_table, desired_table):
        altered_columns = []
        prev_column_name = None
        existing_columns_dict = self._columns_to_dict(existing_table.columns)
        for desired_column in desired_table.columns:
            column_name = desired_column.name
            if column_name not in existing_columns_dict:
                desired_column.alter_type = "add"
                if prev_column_name:
                    desired_column.prev_column_name = prev_column_name
                altered_columns.append(desired_column)
            else:
                existing_column = existing_columns_dict[column_name]
                if desired_column != existing_column:
                    desired_column.alter_type = "modify"
                    altered_columns.append(desired_column)
            prev_column_name = column_name
        return altered_columns

    def _find_obsolete_columns(self, existing_table, desired_table):
        altered_columns = []
        desired_columns_dict = self._columns_to_dict(desired_table.columns)
        for existing_column in existing_table.columns:
            if existing_column.name not in desired_columns_dict:
                existing_column.alter_type = "drop"
                altered_columns.append(existing_column)
        return altered_columns

    def _build_alter_table_queries(self, existing_table, desired_table):
        altered_columns = []
        altered_columns += self._find_missing_or_modified_columns(existing_table, desired_table)
        altered_columns += self._find_obsolete_columns(existing_table, desired_table)
        queries = []
        if altered_columns:
            alter_query = self._ch_client.generate_alter_query(existing_table, altered_columns)
            if not alter_query:
                self._module.fail_json(msg="Could not generate ALTER TABLE query for the following columns: %s" %
                                       ", ".join([column.name for column in altered_columns]))
            queries.append(alter_query)
        return queries

    def _build_drop_table_queries(self, existing_table, desired_table):
        return [self._ch_client.generate_drop_table_query(existing_table)]


def validate_module_params(module):
    if module.params['state'] == "present":
        for param in ['columns', 'engine']:
            if not module.params.get(param, None):
                module.fail_json(msg="'%s' parameter is mandatory with state 'present'" % param)


def main():
    module_args = dict(
        database=dict(type='str', required=False, default='default', aliases=['db']),
        table=dict(type='str', required=True, aliases=['name']),
        columns=dict(type='list', required=False, default=[], aliases=['cols']),
        engine=dict(type='str', required=False),
        url=dict(type='str', required=False, default='http://localhost:8123'),
        create_db=dict(type='bool', required=False, default=True),
        state=dict(default='present', required=False, choices=['present', 'absent'])
    )

    result = dict(
        changed=False,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    validate_module_params(module)

    ch_client = CHClient(module)
    existing_table = CHTable(module, ch_client, existing=True)
    desired_table = CHTable(module, ch_client)

    diff_calculator = CHTableDiffCalculator(module, ch_client)
    diff = diff_calculator.calculate(existing_table, desired_table)
    if diff:
        result['diff'] = dict(diff)
        result['changed'] = True
        if not module.check_mode:
            for query in diff.queries:
                ch_client.execute_query(query)

    module.exit_json(**result)


if __name__ == '__main__':
    main()
