---
- set_fact:
    uss_zx_described_inventory_groups: "{{ hostvars[inventory_hostname]['group_names'] | intersect(uss_zx_services) }}"
    uss_zx_default_groups:
      - "{{ uss_zx_common_group }}"
    uss_zx_default_templates:
      - TMPL_LINUX
    uss_zx_default_ports: []
    uss_zx_default_external_ports: []
    uss_zx_default_packages: []
    uss_zx_default_files: []
    uss_zx_default_interfaces:
      - type: agent
        port: 10050
    uss_zx_default_neighbors: []
    uss_zx_default_macros: "{{ [{ 'name': 'MONITORING_PREFIX', 'value': uss_monitoring_prefix }] + uss_zx_macros }}"
  tags:
    - zabbix_groups

- set_fact:
    uss_zx_groups: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'groups') | select('defined') | sum(start=uss_zx_default_groups) | list }}"
    uss_zx_templates: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'templates') | select('defined') | sum(start=uss_zx_default_templates) | list }}"
    uss_zx_ports: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'ports') | select('defined') | sum(start=uss_zx_default_ports) | list }}"
    uss_zx_external_ports: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'external_ports') | select('defined') | sum(start=uss_zx_default_external_ports) | list }}"
    uss_zx_packages: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'packages') | select('defined') | sum(start=uss_zx_default_packages) | list }}"
    uss_zx_files: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'files') | select('defined') | sum(start=uss_zx_default_files) | list }}"
    uss_zx_interfaces: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'interfaces') | select('defined') | sum(start=uss_zx_default_interfaces) | list }}"
    uss_zx_neighbors: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'neighbors') | select('defined') | sum(start=uss_zx_default_neighbors) | list }}"
    uss_zx_macros: "{{ uss_zx_described_inventory_groups | map('extract', uss_zx_services, 'macros') | select('defined') | sum(start=uss_zx_default_macros) | list }}"
  tags:
    - zabbix_groups

- set_fact:
    uss_zx_groups_all: "{{ (uss_zx_services | json_query('*.groups[]') + uss_zx_default_groups) | unique }}"
    uss_zx_interfaces_full: |
      {{ uss_zx_interfaces_full | default([]) + [{
            'type': uss_zx_interface_types[item.type],
            "main": item.main | default(1),
            "useip": 1,
            "ip": zx_host_address,
            "dns": "",
            "port": item.port
      }] }}
  with_items: "{{ uss_zx_interfaces }}"
  tags:
    - zabbix_groups

- name: Create groups
  zabbix_group:
    server_url: "https://{{ zx_server }}"
    login_user: "{{ zx_api_user }}"
    login_password: "{{ zx_api_password }}"
    state: present
    host_groups: "{{ uss_zx_groups_all }}"
  run_once: yes
  delegate_to: localhost
  tags:
    - zabbix_groups

- name: Create host
  zabbix_host:
    server_url: "https://{{ zx_server }}"
    login_user: "{{ zx_api_user }}"
    login_password: "{{ zx_api_password }}"
    host_name: "{{ zx_host }}"
    state: present
    link_templates: "{{ uss_zx_templates }}"
    host_groups: "{{ uss_zx_groups }}"
    interfaces: "{{ uss_zx_interfaces_full }}"
    inventory_mode: automatic
  delegate_to: localhost
  register: uss_zx_create_host
  tags:
    - zabbix_create_host

- name: Create host macros
  zabbix_hostmacro:
    server_url: "https://{{ zx_server }}"
    login_user: "{{ zx_api_user }}"
    login_password: "{{ zx_api_password }}"
    host_name: "{{ zx_host }}"
    macro_name: "{{ item.name }}"
    macro_value: "{{ item.value }}"
  delegate_to: localhost
  with_items: "{{ uss_zx_macros }}"

- name: Create map
  zabbix_map:
    server_url: "https://{{ zx_server }}"
    login_user: "{{ zx_api_user }}"
    login_password: "{{ zx_api_password }}"
    state: present
    name: "{{ uss_zx_common_group }} (full)"
    data: "{{ lookup('template', 'map_full.dot.j2') }}"
    width: 1200
    height: 800
    default_image: Cloud_(24)
    expand_problem: no
    highlight: no
    label_type: label
  delegate_to: "{{ zx_sender_host }}"
  run_once: yes
  environment:
    PYTHONHTTPSVERIFY: 0

- name: Set flag if at least 1 host has got into 'changed' state
  set_fact:
    uss_zx_hosts_changed: yes
  delegate_to: localhost
  delegate_facts: yes
  when:
    - uss_zx_create_host is defined
    - uss_zx_create_host.changed

- name: Wait for newly created items to get to configuration cache
  pause:
    minutes: 2
  run_once: yes
  when: hostvars.localhost.uss_zx_hosts_changed | default(False) | bool

- include_role:
    name: uss-zabbix-sender
  vars:
    zx_sender_template: tmpl_linux.j2
  run_once: yes
